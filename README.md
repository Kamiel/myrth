# MYRTH
## color theme
### for EMACS
***
#### Eye-candy light clear minimalistic color theme based on base16-solarized-light theme.
***
![ScreenShot](https://bitbucket.org/Kamiel/myrth/raw/master/screenshot.png)
***
### Installation
###### With [el-get](https://github.com/dimitri/el-get)

    (:name myrth-theme
           :description "Eye-candy light clear minimalistic color theme."
           :website "https://bitbucket.org/Kamiel/myrth"
           :type git
           :url "https://Kamiel@bitbucket.org/Kamiel/myrth.git"
           :post-init (add-to-list 'custom-theme-load-path
                                   default-directory))

###### Manual
Copy `myrth-theme.el` to `custom-theme-directory`  
or add path to your theme directory to `custom-theme-load-path`.

### Enable
Select theme with `customize-themes` or manually

    (setq custom-enabled-themes '(myrth))

***
### Inspired by themes
- base16
- tomorrow
- solarized light
- adwaita
- humane
- leuven
